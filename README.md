# OpenStreetMap

## Description

Data Exports from OpenStreetMap done with the [HOT Export Tool](https://export.hotosm.org/fr/v3/exports/).

## Source & Credits

[OpenStreetMap](https://openstreetmap.org) and contributors.

## License

Open Database License (ODbL)

## Selected Datasets

| Name                                                             | Description                                                                           | Type | Format | Size | License |
| ---------------------------------------------------------------- | ------------------------------------------------------------------------------------- | ------ | ---- | ---- | ------- |
| [Whole Country](/cameroon-osm-2019-10-18.pbf)             | [Data files provided by Geofabrik](http://download.geofabrik.de/africa/cameroon.html) | Vector       | [PBF](https://wiki.openstreetmap.org/wiki/PBF_Format)     | 143 MB     |  ODbL       |
| [Primary Roads](cameroon-osm-highway_primary-2019-10-18.geojson) | [highway=primary](https://wiki.openstreetmap.org/wiki/Tag:highway%3Dprimary)          |  Vector      | GeoJSON     |  7 MB    |  ODbL       |
